//
//  ViewController.m
//  iOS-UIImageView
//
//  Created by 吴开杰 on 2020/2/16.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *tomcat;
- (IBAction)shopping_add:(id)sender;
- (IBAction)shopping:(id)sender;
- (IBAction)fire:(id)sender;
- (IBAction)clock_time:(id)sender;
- (IBAction)clock:(id)sender;
- (IBAction)index:(id)sender;
@property(strong, nonatomic) NSMutableArray *imgArray;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.imgArray = [NSMutableArray array];
    
}


- (IBAction)shopping_add:(id)sender {
    
    if (self.imgArray.count >0) {
        [self.imgArray removeAllObjects];
    }
    
    for (int i = 1; i <= 3; i++) {
        
        NSString *imgName = [NSString stringWithFormat:@"tom%02d.jpg",i];
        NSLog(@"%@",imgName);
        UIImage *img = [UIImage imageNamed:imgName];
        [self.imgArray addObject:img];
        
    }
    
    self.tomcat.animationImages = self.imgArray;
    self.tomcat.animationRepeatCount = 1;
    self.tomcat.animationDuration = self.imgArray.count * 0.175;
    [self.tomcat startAnimating];
    
    
    
    
    
    
}

- (IBAction)shopping:(id)sender {
}

- (IBAction)fire:(id)sender {
}

- (IBAction)clock_time:(id)sender {
}

- (IBAction)clock:(id)sender {
}

- (IBAction)index:(id)sender {
}
@end
