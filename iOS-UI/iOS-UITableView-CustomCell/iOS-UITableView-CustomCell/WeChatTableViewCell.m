//
//  WeChatTableViewCell.m
//  iOS-UITableView-CustomCell
//
//  Created by 吴开杰 on 2020/2/18.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "WeChatTableViewCell.h"

@implementation WeChatTableViewCell


- (void)configCell:(WeChat *)wechat{
    self.pic.image = [UIImage imageNamed:wechat.picName];
    self.title.text = wechat.titleText;
    self.time.text = wechat.timeText;
    self.title.text = wechat.titleText;
    self.content.text = wechat.contextText;
}
@end
