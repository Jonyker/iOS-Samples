//
//  WeChat.h
//  iOS-UITableView-CustomCell
//
//  Created by 吴开杰 on 2020/2/18.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WeChat : NSObject

@property(nonatomic,copy) NSString *picName;
@property(nonatomic,copy) NSString *titleText;
@property(nonatomic,copy) NSString *timeText;
@property(nonatomic,copy) NSString *contextText;

@property(strong,nonatomic) NSMutableArray *wechats;
-(NSMutableArray *) getModels;
@end

NS_ASSUME_NONNULL_END
