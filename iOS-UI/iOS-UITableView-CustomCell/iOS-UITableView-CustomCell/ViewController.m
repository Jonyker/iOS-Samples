//
//  ViewController.m
//  iOS-UITableView-CustomCell
//
//  Created by 吴开杰 on 2020/2/18.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"
#import "WeChatTableViewCell.h"
#import "WeChatTableViewCellXIBTableViewCell.h"
#import "WeChat.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic) NSMutableArray *wechats;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.rowHeight = 100.0;
    // WeChat中获取数据
    self.wechats = [[[WeChat alloc]init] getModels];
    
    // 去掉底部空白行
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    // 使用xib的方式，需要建立关联
    // 创建Nib，名称是nib文件的名称，bundle是当前bundle，传递nil
    UINib *nib = [UINib nibWithNibName:@"WeChatTableViewCellXIBTableViewCell" bundle: nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"cba"];
    
    
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    
    
    // 第一种方式：通过自定义
    // WeChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"abc"];
    
    // 第二种方式：使用xib的方式创建Cell
    WeChatTableViewCellXIBTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cba"];
    [cell configCell:[self.wechats objectAtIndex:indexPath.row]];
    
    
    return cell;
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"item的数量：%ld",(long)self.wechats.count);
    
    return self.wechats.count;
    
}


@end
