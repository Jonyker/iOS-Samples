//
//  WeChat.m
//  iOS-UITableView-CustomCell
//
//  Created by 吴开杰 on 2020/2/18.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "WeChat.h"


@interface WeChat()

@property(strong,nonatomic) NSArray *pics;
@property(strong,nonatomic) NSArray *titles;
@property(strong,nonatomic) NSArray *times;
@property(strong,nonatomic) NSArray *contents;

@end

@implementation WeChat

-(NSArray *) wechats {
    if(_wechats == nil){
        
        _wechats = [NSMutableArray array];
        for (int i =0; i< self.titles.count; i++) {
  
            WeChat *wechat = [[WeChat alloc]init];
            wechat.picName = self.pics[i];
            wechat.titleText = self.titles[i];
            wechat.timeText = self.times[i];
            wechat.contextText = self.contents[i];
  
            [_wechats addObject:wechat];
            
        }
    }
    NSLog(@"%ld",(long)_wechats.count);
    
    return _wechats;
}

-(NSArray *) pics {
    if(_pics == nil){
        _pics = @[@"format_pdf", @"format_ppt", @"format_word"];
    }
    return _pics;
}

-(NSArray *) titles {
    if(_titles == nil){
        _titles = @[@"PDF文档标题", @"PPT文档标题", @"Word文档标题"];
    }
    return _titles;
}

-(NSArray *) times {
    if(_times == nil){
        _times = @[@"10:00", @"11:20", @"17:43"];
    }
    return _times;
}

-(NSArray *) contents {
    if(_contents == nil){
        _contents = @[@"PDF文档内容啊", @"PPT文档内容啊", @"Word文档内容啊"];
    }
    return _contents;
}

-(NSMutableArray *) getModels{
    return self.wechats;
}
@end
