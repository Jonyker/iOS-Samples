//
//  WeChatTableViewCell.h
//  iOS-UITableView-CustomCell
//
//  Created by 吴开杰 on 2020/2/18.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeChat.h"

NS_ASSUME_NONNULL_BEGIN

@interface WeChatTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *pic;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *time;

-(void)configCell:(WeChat *)wechat;

@end

NS_ASSUME_NONNULL_END
