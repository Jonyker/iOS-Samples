//
//  UserModel.h
//  iOS-UITableView-Model
//
//  Created by 吴开杰 on 2020/2/18.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserModel : NSObject


@property (nonatomic,copy) NSString *username;
@property (nonatomic,copy) NSString *passpword;


@end

NS_ASSUME_NONNULL_END
