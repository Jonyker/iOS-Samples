//
//  ViewController.m
//  iOS-UITableView-UIRefreshControl
//
//  Created by 吴开杰 on 2020/2/18.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIRefreshControl *control = [[UIRefreshControl alloc]init];
    
    control.attributedTitle = [[NSAttributedString alloc]initWithString:@"下拉刷新"];
    control.tintColor = UIColor.redColor;
    
    self.tableView.refreshControl = control;
    
}


@end
