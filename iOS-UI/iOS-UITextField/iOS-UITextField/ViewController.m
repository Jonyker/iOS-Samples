//
//  ViewController.m
//  iOS-UITextFiled
//
//  Created by 吴开杰 on 2020/2/16.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)textChange:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *tf;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//    UITextField *textField = [UITextField alloc];
    
}

// 实时获取文本的变化
- (IBAction)textChange:(id)sender {
    
    UITextField* tf = (UITextField*)sender;
    NSLog(@"%@",tf.text);
    
}

// 触摸UIView，就会触发这个方法
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    // 触摸UIView方法，关闭软键盘
    // [self.view endEditing:YES];
    // 通过UITextField自己的方法，关闭软键盘，失去第一响应者
    [self.tf resignFirstResponder];
    
}

@end
