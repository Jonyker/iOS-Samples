//
//  ViewController.m
//  iOS-Switch
//
//  Created by 吴开杰 on 2020/2/16.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *bulb;
- (IBAction)changeBulb:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)changeBulb:(id)sender {
    
    UISwitch *swi = (UISwitch *)sender;
    if(swi.isOn){
        // 如果是png类型的图片，省略后缀
        self.bulb.image = [UIImage imageNamed:@"bulb_on"];
    } else {
        self.bulb.image = [UIImage imageNamed:@"bulb_off"];
    }
    
    
}
@end
