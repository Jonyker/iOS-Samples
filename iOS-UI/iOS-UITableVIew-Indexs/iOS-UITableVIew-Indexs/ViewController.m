//
//  ViewController.m
//  iOS-UITableVIew-Indexs
//
//  Created by 吴开杰 on 2020/2/18.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(strong,nonatomic) NSArray * sectionTitles;
@property(strong,nonatomic) NSArray<NSArray *> * contentsArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ViewController


- (NSArray<NSArray *> *)contentsArray{
    // 懒加载，延迟加载，在界面需要数据的时候加载数据，而不是盲目的将无用的数据加载到内存中
    if(_contentsArray == nil){
        _contentsArray = [[NSArray alloc] initWithObjects:@[@"a1",@"a2",@"a3"],@[@"b1",@"b2",@"b3"],@[@"c1",@"c2",@"c3"], nil];
    }
    return _contentsArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.sectionTitles = [[NSArray alloc] initWithObjects:@"A",@"B",@"C", nil];

    
    // 索引的文字颜色
    self.tableView.sectionIndexColor = UIColor.systemPinkColor;
    // 索引的背景的颜色
    self.tableView.sectionIndexBackgroundColor = UIColor.grayColor;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return self.sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.contentsArray[section].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"abc"];
    cell.textLabel.text = self.contentsArray[indexPath.section][indexPath.row];
    return cell;
    
}

// 显示Section的title
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return self.sectionTitles[section];
}

// 显示右侧索引
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.sectionTitles;
}

// 点击索引监听的事件
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    NSLog(@"您点击的索引是：%@",title);
    // 返回当前的section索引
    return index;
}

@end
