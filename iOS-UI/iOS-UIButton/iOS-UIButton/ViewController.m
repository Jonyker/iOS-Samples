//
//  ViewController.m
//  iOS-UIButton
//
//  Created by 吴开杰 on 2020/2/16.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
- (IBAction)login:(id)sender;
- (IBAction)regis:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)login:(id)sender {
    NSString *name = self.username.text;
    NSString *pwd = self.password.text;
    
    NSLog(@"用户名：%@，密码：%@",name,pwd);
    if([name isEqualToString:@"admin"] && [pwd isEqualToString:@"12345"]){
        NSLog(@"登录成功！");
    } else {
        NSLog(@"登录失败，用户名或密码不正确！");
    }
    
}

- (IBAction)regis:(id)sender {
    NSLog(@"用户注册！");
}



@end
