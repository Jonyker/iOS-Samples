//
//  ViewController.m
//  iOS-UITableView
//
//  Created by 吴开杰 on 2020/2/17.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

// 注意，需要在stroyboard设置连接线，方式类似outlet
@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic) NSMutableArray *data;
- (IBAction)btn_add:(id)sender;
- (IBAction)btn_finish:(id)sender;
@end

// UITableView组成：
// 一个tableView HeaderView + 若干个Section + tableView FooterView
// 一个Section: section HeaderView + 若干个cell + section FooterView

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSArray *one = @[@"iPhone 1",@"iPhone 2",@"iPhone 3",@"iPhone 4",@"iPhone 5",@"iPhone 6",@"iPhone 7",@"iPhone 8",@"iPhone 9",@"iPhone 10",@"iPhone 11",@"iPhone 12"];
    
    self.data = [NSMutableArray arrayWithArray:one];
    
    self.tableView.rowHeight = 80.0;
    
    UIRefreshControl *control = [[UIRefreshControl alloc]init];
    
    control.attributedTitle = [[NSAttributedString alloc]initWithString:@"下拉刷新"];
    control.tintColor = UIColor.redColor;
    [control addTarget:self action:@selector(refreTableView) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.refreshControl = control;
    
    
}

// 添加下拉刷新事件
-(void)refreTableView{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.data insertObject:@"1212121" atIndex:0];
        [self.tableView reloadData];
        if([self.tableView.refreshControl isRefreshing]){
            [self.tableView.refreshControl endRefreshing];
        }
    });
    
}

// 一共多少个Section，默认只有一个Section，可以不进行实现
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

// 一个Section有多少行（Cell）
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // 需要动态设置数据的数量，否则在编辑的时候回报错
    return self.data.count;
}

// 每行（Cell）显示的内容
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    // UITableViewCell提供的默认内部结构:
    // 直接视频是contentView，然后包含3个UIView，分imageView,textLabel,DetailTextLabel
    
//    // 重用方式一：（代码设置）
//    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"abc"];
//    if(cell == nil){
//       // iOS系统内置了4中样式的TableView Cell，在这里设置的时候需要和storyboard里面设置的style保持一致。
//        // 设置reuseIdentifier参数，是为了在重用池中查找是否有可以重用的Cell
//        UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"abc"];
//    }
//    cell.imageView.image = [UIImage imageNamed:@"icon_file"];
//    cell.textLabel.text = @"AAA";
//    cell.detailTextLabel.text = @"bbb";
    
    
    NSLog(@"%ld",(long)indexPath.row);
    
    // 重用方式二：（stroyboad设置Identifier）(建议使用)
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"abc"];
    cell.textLabel.text = self.data[indexPath.row];
    
    // 单元格重用，会导致数据混乱
    
    return cell;
}

// 数据源方法
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return @"头部视图";
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    
    return @"顶部视图";
}

// 代理方法
// 不建议使用代理方法，设置行高，因为调用的次数太多，直接设置self.tableView中设置高度正好
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 80.0;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 60.0;
}

// 设置tableView的头部，和底部
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section



// 处理tableView的点击事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"%@",self.data[indexPath.row]);
    
}

// 滑动删除
// 1.改行能不能编辑
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
// 2.提交编辑
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"提交删除的是第 %ld 行",indexPath.row);
    
    // 需要先判断编辑风格，判断是删除风格，还是增加风格
    if(editingStyle == UITableViewCellEditingStyleDelete){
        // 注意顺序，需要先删除数据源，再删除界面上的那一行
        // 1.同时需要删除数据员中的数据
        [self.data removeObjectAtIndex:indexPath.row];
        // 2.删除界面上的那一行
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if(editingStyle == UITableViewCellEditingStyleInsert){
        
        // 1.添加数据
        [self.data insertObject:@"iphone new" atIndex:indexPath.row];
        
        // 局部刷新，仅仅刷新更新的那一行，性能更高，建议使用
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
        // 简单粗暴，刷新整个表格
        // [self.tableView reloadData];
        
    }

    
}
// 3.设置删除文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}

- (IBAction)btn_add:(id)sender {
    
    // 向TableView中添加数据
    // 1.TableView必须是可以编辑的，重写canEditRowAtIndexPath代理方法
    
    // 2.设置进入编辑状态，如果是删除风格，就是出现删除按钮，如果是添加风格，就会出现绿色添加按钮
    [self.tableView setEditing:YES];
    
    // 3.设置编辑风格
    
}

- (IBAction)btn_finish:(id)sender {
    [self.tableView setEditing:NO];
}

// 开启Cell移动的能力
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{

    // 删除以后，滑动上去还存在,需要对数据源进行改造
    NSString *cellData = self.data[sourceIndexPath.row];
    // 1.移除需要移动的Cell（移除原始位置的数据）
    [self.data removeObjectAtIndex:sourceIndexPath.row];
    // 2.往目标位置插入一个数据
    [self.data insertObject:cellData atIndex:destinationIndexPath.row];
    
    
}

// 设置编辑的风格
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 默认是删除风格

    // 设置添加风格
    return UITableViewCellEditingStyleInsert;
}
@end
