//
//  ViewController.m
//  iOS-UIDatePicker
//
//  Created by 吴开杰 on 2020/2/17.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)getDate:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UITextField *inputBirth;

@property (strong, nonatomic) UIDatePicker *picker;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // 1.创建日期选择器
    self.picker = [[UIDatePicker alloc] init];
    self.picker.datePickerMode = UIDatePickerModeDate;
    self.picker.locale = [NSLocale localeWithLocaleIdentifier:@"ZH_cn"];
    // 2.设置输入框的输入类型为日期选择器
    self.inputBirth.inputView = self.picker;
    // 3.监听选择的日期
    [self.picker addTarget:self action:@selector(getBirth:) forControlEvents:UIControlEventValueChanged];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    
}

-(void) getBirth:(UIDatePicker *)picker{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    // 如果是24小时 将hh->HH
    [formater setDateFormat:@"yyyy-MM-dd"];
    self.inputBirth.text = [formater stringFromDate:picker.date];
    
}


- (IBAction)getDate:(id)sender {
    
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    // 如果是24小时 将hh->HH
    [formater setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    UIDatePicker * datePicker = (UIDatePicker *)sender;
    NSLog(@"%@",datePicker.date);
    
    // 日期格式化
    self.date.text = [formater stringFromDate: datePicker.date];
    
    
    
}
@end
