//
//  ViewController.m
//  iOS-UIStepper
//
//  Created by 吴开杰 on 2020/2/16.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;
- (IBAction)changeStepper:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)changeStepper:(id)sender {
    UIStepper * stepper = (UIStepper *)sender;
    
    NSString *value = [NSString stringWithFormat:@"您购买了%d件商品~",(int)stepper.value];
    self.label.text = value;
    
}
@end
