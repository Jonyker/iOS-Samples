//
//  ViewController.m
//  iOS-UILabel
//
//  Created by 吴开杰 on 2020/2/16.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    label.text = @"Hello iOS UILabel Hello iOS UILabel ";
    label.font = [UIFont systemFontOfSize:20];
    label.textColor = [UIColor blueColor];
    label.numberOfLines = 0;
    [self.view addSubview:label];
    
    
}


@end
