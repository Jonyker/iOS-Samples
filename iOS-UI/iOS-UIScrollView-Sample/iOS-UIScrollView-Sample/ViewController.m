//
//  ViewController.m
//  iOS-UIScrollView-Sample
//
//  Created by 吴开杰 on 2020/2/17.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomImg;
@property (weak, nonatomic) IBOutlet UIScrollView *bannerScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // 获取屏幕的宽度
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    for (int i =0 ; i<5; i++) {
        CGRect frame = CGRectMake(width * i, 0, width, 241);
        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:frame];
        
        NSString *imgName = [NSString stringWithFormat:@"girl_%02d.jpg",i];
        
        imgView.image = [UIImage imageNamed:imgName];
        [self.bannerScrollView addSubview: imgView];
        
    }
    
    // 设置的banner的大小
    self.bannerScrollView.contentSize = CGSizeMake(width * 5, 0);
    
    self.bannerScrollView.delegate = self;
    
    // 获取最后一个元素的坐标，然后动态设置ScrollView的高度
    CGFloat y = CGRectGetMaxY(self.bottomImg.frame);
    
    // 水平不能滚动 x需要设置为 0
    self.scrollView.contentSize = CGSizeMake(0, y + 20);
    
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    
    // 获取屏幕的宽度
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    CGFloat offset = self.bannerScrollView.contentOffset.x;
    
    NSInteger index = offset/width;
    
    self.pageControl.currentPage = index;
    
}
@end
