//
//  ViewController.m
//  iOS-UISegmentControl
//
//  Created by 吴开杰 on 2020/2/16.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *img;
- (IBAction)changeSegment:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *yelllowIndicator;
@property (weak, nonatomic) IBOutlet UIProgressView *progress;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.yelllowIndicator startAnimating];
    // 延时3秒结束转动
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.indicator stopAnimating];
        
    });
    
    // 动画指定3秒
    [UIView animateWithDuration:3.0 animations:^{
        
        [self.progress setProgress:1.0 animated:YES];
        
    }];
}


- (IBAction)changeSegment:(id)sender {
    
       UISegmentedControl * segment = (UISegmentedControl *)sender;
       
       if(segment.selectedSegmentIndex == 0){
           self.img.image = [UIImage imageNamed:@"tom01.jpg"];
       } else if(segment.selectedSegmentIndex == 1){
           self.img.image = [UIImage imageNamed:@"tom02.jpg"];
       } else if(segment.selectedSegmentIndex == 2){
           self.img.image = [UIImage imageNamed:@"tom03.jpg"];
       }
    
}
@end
