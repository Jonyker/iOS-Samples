//
//  ViewController.m
//  iOS-UISlider
//
//  Created by 吴开杰 on 2020/2/16.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)changeSlider:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)changeSlider:(id)sender {
    
    UISlider *slider = (UISlider *)sender;
    if(slider.value < 35.0){
        self.img.image = [UIImage imageNamed:@"tom01.jpg"];
    } else if(slider.value < 70.0){
        self.img.image = [UIImage imageNamed:@"tom02.jpg"];
    } else if(slider.value < 100.0){
        self.img.image = [UIImage imageNamed:@"tom03.jpg"];
    }
    
}
@end
