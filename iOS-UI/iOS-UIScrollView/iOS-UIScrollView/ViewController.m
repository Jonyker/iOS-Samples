//
//  ViewController.m
//  iOS-UIScrollView
//
//  Created by 吴开杰 on 2020/2/17.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,nonatomic) UIImageView *imageView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.scrollView.delegate = self;
    
    // 滚动内容的大小,如果不设置在ScrollView中的内容不能进行滚动
    self.scrollView.contentSize = CGSizeMake(600, 800);
    // 设置内容的偏移量，就是将内容的100，200的位置，挪到ScrollView得左上角坐标原点
    self.scrollView.contentOffset = CGPointMake(100, 200);
    // 设置内容的边距
    self.scrollView.contentInset = UIEdgeInsetsMake(100, 100, 300, 100);
    // 设置缩放最大，和最小的比例
    self.scrollView.maximumZoomScale = 2.0;
    self.scrollView.minimumZoomScale = 0.5;
    
    self.imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tom02.jpg"]];
    [self.scrollView addSubview:self.imageView];

}

// 只要滚动就开始调用
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    
}

// 只要缩放就调用
- (void)scrollViewDidZoom:(UIScrollView *)scrollView API_AVAILABLE(ios(3.2)){
    NSLog(@"%s",__func__);
    
}

// 开始拖拽的时候调用
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    
}

// 拖拽即将结束的时候调用
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset API_AVAILABLE(ios(5.0)){
    NSLog(@"%s",__func__);
    
}

// 拖拽完成的时候调用
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    NSLog(@"%s",__func__);
    
}
// 开始减速的时候调用
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    
}

// 减速完成的时候调用
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    
}






// 返回需要缩放的控件
- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    return self.imageView;
}

// 即将开始缩放
- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view API_AVAILABLE(ios(3.2)){
    NSLog(@"%s",__func__);
}

// 缩放完成
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale{
    NSLog(@"%s",__func__);
}





@end
