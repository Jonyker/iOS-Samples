//
//  ViewController.m
//  iOS-UIPickerView
//
//  Created by 吴开杰 on 2020/2/17.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) NSArray *data1;
@property (strong, nonatomic) NSArray *data2;
@property (weak, nonatomic) IBOutlet UILabel *itemSelect;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.data1 = @[@"AA",@"BB",@"CC",@"DD"];
    self.data2 = @[@"米饭",@"稀饭 ",@"面条",@"水饺"];
    
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;

}

// 选择器有多少列
- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    
    return 2;
}

// 选择器有多少行
- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if(component == 0){
        return self.data1.count;
    } else {
        return self.data2.count;
    }
    
}
// 每一行的标题
// 多次调用，每调用一个返回一个Row的标题
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSLog(@"---------title");
    
    if(component == 0){
        return self.data1[row];
    } else {
        return self.data2[row];
    }
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(component == 0){
        self.itemSelect.text = self.data1[row];
    } else {
        self.itemSelect.text = self.data2[row];
    }
    
}

@end
