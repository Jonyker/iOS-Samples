//
//  ViewController.m
//  iOS-UICollectionView
//
//  Created by 吴开杰 on 2020/2/18.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    
    CGFloat w = (UIScreen.mainScreen.bounds.size.width - 3*5) * 0.5;
    
    layout.itemSize = CGSizeMake(w, 250.0);
    layout.minimumLineSpacing = 5.0;
    layout.minimumInteritemSpacing = 5.0;
    
    // 设置item的边距，如果不设置上面的5*3，会被挤在中间，设置了边距正好将中间的15，挤压成5
    layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
    self.collectionView.collectionViewLayout = layout;
     
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    
    
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"abc" forIndexPath:indexPath];
    
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    return 10;
    
}

@end
