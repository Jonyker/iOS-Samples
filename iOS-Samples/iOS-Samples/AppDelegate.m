//
//  AppDelegate.m
//  iOS-Samples
//
//  Created by 吴开杰 on 2019/6/3.
//  Copyright © 2019年 吴开杰. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

// 程序运行的时候开始调用
// 默认情况下，检测程序 是否使用了storyboard作为界面，并设置而了ViewController
// 如果存在storyboard界面，会查找info.plist文件中的main storyboard file，然后寻找箭头指向的那个界面（勾选is initial View Controller的）
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSLog(@"%s",__func__);
    
//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//
//    UITabBarController *tabbarController = [[UITabBarController alloc] init];
//
//    UIViewController *controller = [[UIViewController alloc] init];
//    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
//
//    navController.view.backgroundColor = [UIColor redColor];
//    navController.tabBarItem.title = @"新闻";
//
//
//    UIViewController *controller2 = [[UIViewController alloc] init];
//    controller2.view.backgroundColor = [UIColor greenColor];
//    controller2.tabBarItem.title = @"视频";
//
//    UIViewController *controller3 = [[UIViewController alloc] init];
//    controller3.view.backgroundColor = [UIColor blueColor];
//    controller3.tabBarItem.title = @"推荐";
//
//    UIViewController *controller4 = [[UIViewController alloc] init];
//    controller4.view.backgroundColor = [UIColor lightGrayColor];
//    controller4.tabBarItem.title = @"我的";
//
//    [tabbarController setViewControllers:@[navController,controller2,controller3,controller4]];
//
//    // 根部容器，指定tabbarController来管理
//    self.window.rootViewController = tabbarController;
//    [self.window makeKeyWindow];
    
    return YES;
}

// 程序进入非活动状态
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    NSLog(@"%s",__func__);
}

// 程序进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"%s",__func__);
}

// 程序即将进入前台
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    NSLog(@"%s",__func__);
}

// 程序进入活动状态
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"%s",__func__);
}

// 程序结束
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"%s",__func__);
}


@end
