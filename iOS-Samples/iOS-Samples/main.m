//
//  main.m
//  iOS-Samples
//
//  Created by 吴开杰 on 2019/6/3.
//  Copyright © 2019年 吴开杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        // 第三个参数传递 必须为UIApplication或其子类，代表当前app本身，并且参数为 nil 的时候，默认为 @"UIApplication"
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
