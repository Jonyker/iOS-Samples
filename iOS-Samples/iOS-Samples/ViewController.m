//
//  ViewController.m
//  iOS-Samples
//
//  Created by 吴开杰 on 2019/6/3.
//  Copyright © 2019年 吴开杰. All rights reserved.
//

#import "ViewController.h"

//----------------------TestView----------------------start
@interface TestView : UIView
@end

@implementation TestView
- (instancetype)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}
// View的生命周期
- (void)willMoveToSuperview:(nullable UIView *)newSuperview{
    [super willMoveToSuperview:newSuperview];
}
- (void)didMoveToSuperview{
    [super didMoveToSuperview];
}
- (void)willMoveToWindow:(nullable UIWindow *)newWindow{
    [super willMoveToWindow:newWindow];
}
- (void)didMoveToWindow{
    [super didMoveToWindow];
}
@end
//----------------------TestView----------------------end


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *contectView;
// 并不是所有的控件都可以拖拽BIBAction，只有继承UICtrol的控件才可以
- (IBAction)click:(id)sender;

@end

// 提供两种ViewController和View连接的桥梁，IBoutlet，IBAction（连接以后它们都是实心）
// IBoutlet：（连接）
// 1.将StoryBoard和ViewController分栏，会将代码和视图同事显示出来（通过点击右边上部两个交叉的圆圈）
// 2.然后找到ViewController的@interface ViewController ()位置
// 3.按住ctrl键，拖动UIView到@interface ViewController ()和@end之间，会弹出一个弹窗
// 4.弹窗中connection是Outlet，会自动推断出UIView类型，需要做的是设置一个name名称
// 5.最后会生成一个@property,这样就将UIView和ViewController连接在一起【推荐使用这种方式】
//
// IBAction：（动作）
// 1.IBAction的连接方式相同
// 2.弹窗设置不同，弹窗中connection是Action
// 3.需要设置事件的名称
// 4.默认会将这个实践进行实现
//
// IBoutlet，IBAction会有个错误：
// 1.如果在代码里view3修改成了view4，会导致在StoryBoard中找不到view4
// 2.查看StoryBoard，点击Show the Contections spectot（圆圈里带箭头的），会发现还是view3
// 3.解决办法，是需要将StoryBoard中的view3修改成view4，或者删除，重新连接
// IBActiony绑定的事件名称也是同样的道理



// 视图控制器管理UIView
// ViewController里面会有一个UIView（与屏幕一样大），即：self.view，可以代码中可以动态控制
@implementation ViewController

- (instancetype)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

// UIView被ViewController加载到内存的时候调用
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 默认View加载完成，一些资源初始化的工作在viewDidLoad初始化
    
    self.view.backgroundColor = [UIColor cyanColor];
    
    NSLog(@"Hi View");

//    TestView *label2 = [[TestView alloc]initWithFrame:CGRectMake(150, 150, 100, 100)];
//    label2.backgroundColor = [UIColor greenColor];
//    [self.view addSubview:label2];
    
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
//    label.text = @"代码label";
//    label.font = [UIFont systemFontOfSize:20];
//    label.backgroundColor = [UIColor blueColor];
//    [self.view addSubview:label];
    
    
//    CGPoint(x, y)
//    CGSize(width, height)
//    CGRect(坐标，宽高)
//    CGSizeMake(<#CGFloat width#>, <#CGFloat height#>)
    
    
    UIView *view = [[UIView alloc] init];
    // frame 是以父控件左上角为坐标原点
    view.frame = CGRectMake(100, 100, 100, 100);
    // bounds
    // view.bounds = CGRectMake(100, 100, 100, 100);
    view.backgroundColor = [UIColor redColor];
    [self.view addSubview:view];
    // 将位置设置为屏幕的正中间
    view.center = self.view.center;
    
    // 通过在View中设置的tag，寻找与ViewController绑定的StoryBoard中的View
    UIView *tagView = [self.view viewWithTag:101];
    tagView.backgroundColor = [UIColor orangeColor];
    
    //通过iboutlet连接UIVIew和ViewController，然后进行操作UIView
    self.contectView.backgroundColor = [UIColor purpleColor];
    

}

// Called when the view is about to made visible. Default does nothing
// UIView即将显示的时候调用
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
// Called when the view has been fully transitioned onto the screen. Default does nothing
// UIView显示出来的时候调用
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


// 渲染的时候布局子控件
- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
}

// 完成子控件的布局
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
}


// Called when the view is dismissed, covered or otherwise hidden. Default does nothing
// UIView即将消失的时候调用
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
// Called after the view was dismissed, covered or otherwise hidden. Default does nothing
// UIView消失的时候调用
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (IBAction)click:(id)sender {
    NSLog(@"click->12345");
    
}

@end






