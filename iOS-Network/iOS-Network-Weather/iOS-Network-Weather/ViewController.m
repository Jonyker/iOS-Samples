//
//  ViewController.m
//  iOS-Network-Weather
//
//  Created by 吴开杰 on 2020/3/10.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"
#import "Weather.h"
#import "WeatherCell.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong) NSMutableArray<Weather*> *weathers;

@end


@implementation ViewController


- (NSMutableArray<Weather*> *)weathers{
    if(_weathers == nil){
        _weathers = [NSMutableArray arrayWithCapacity:7];
    }
    return _weathers;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // http://192.168.124.175:3000/weather
    
    self.tableView.rowHeight = 100.0;
    self.tableView.tableFooterView = [[UIView alloc]init];
    // 使用xib的方式，需要建立关联
    // 创建Nib，名称是nib文件的名称，bundle是当前bundle，传递nil
    UINib *nib = [UINib nibWithNibName:@"WeatherCell" bundle: nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"weather_cell"];
    
    Weather *weather = [[Weather alloc]init];
    
    [weather getWeather:^(NSMutableArray<Weather*> * data) {
        
        self.weathers = data;
        
        //默认网络请求在自线程 更新界面要回到主线程
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [NSThread  sleepForTimeInterval:1.0];
            
            //刷新界面
            [self.tableView reloadData];
            
        });
    }];
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    WeatherCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"weather_cell"];

    cell.w = self.weathers[indexPath.row];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.weathers.count;
}



@end
