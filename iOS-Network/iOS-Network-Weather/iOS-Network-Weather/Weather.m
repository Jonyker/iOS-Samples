//
//  Weather.m
//  iOS-Network-Weather
//
//  Created by 吴开杰 on 2020/3/12.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "Weather.h"

@implementation Weather


//初始化方法
- (instancetype)initWithDictionary:(NSDictionary *)dic{
    
    if(self = [self init]){
        [self setValuesForKeysWithDictionary:dic];
    }
    
    return self;
}


//属性与字典不匹配时进行改正，不改的话不会崩溃但拿不到值
- (void)setValue:(id)value forKey:(NSString *)key{
    //在这里更改key
    if([key isEqualToString:@"date"]){
        
        key = @"date_y";
    }
    
    [super setValue:value forKey:key];
}


//冗错处理，如果有未定义的字段的话就会走到这里，不重写的话会引起崩溃
- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
    NSLog(@"value:%@,undefineKey:%@",value,key);
}


//获取网络数据，参数是传出去的Block
-(void)getWeather:(void (^)(NSMutableArray<Weather*> *))callback{
    
    //1.创建一个url
    NSString *net = @"http://v.juhe.cn/weather/index?format=2&cityname=芜湖&key=2d2e6e836dbdffac56814bc4d449d507";
    
    //带中文的url进行转换
    net = [net stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURL *url = [NSURL URLWithString:net];
    
    //2.创建一个网络请求（url）
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    
    //request.HTTPMethod = @"POST";
    
    //3.创建网络管理
    NSURLSession *session = [NSURLSession sharedSession];
    
    //4.创建一个网络任务
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        //需要转换成NSHTTPURLResponse
        NSHTTPURLResponse *res = (NSHTTPURLResponse *)response;
        
        NSLog(@"%ld", (long)res.statusCode);
        
        if (error) {
            NSLog(@"有错误");
        } else {

            //JSON（字典）转模型
            NSDictionary *dic =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            //取未来7天天气
            NSArray *future = dic[@"result"][@"future"];
            
            NSMutableArray<Weather *> * weathers = [NSMutableArray arrayWithCapacity:7];
            
            NSLog(@"%ld", future.count);
            for (int i=0; i<future.count; i++) {
                
                NSDictionary *dic = future[i];
                Weather *w = [self initWithDictionary:dic];
                [weathers addObject:w];
                
            }
            
            NSLog(@"%ld", weathers.count);
            
            //网络放到Model以后，必须能和Controller通信，可以用代理、Block和通知，这里用Block
            callback(weathers);
            
        }
        
    }];
    
    //5.开启任务
    [task resume];
    
}





@end
