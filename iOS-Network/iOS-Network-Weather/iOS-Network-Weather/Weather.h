//
//  Weather.h
//  iOS-Network-Weather
//
//  Created by 吴开杰 on 2020/3/12.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Weather : NSObject

@property(nonatomic,copy) NSString* temperature;
@property(nonatomic,copy) NSString* weather;
@property(nonatomic,copy) NSString* wind;
@property(nonatomic,copy) NSString* week;
@property(nonatomic,copy) NSString* date_y;
@property(nonatomic,copy) NSDictionary* weather_id;

//初始化方法
- (instancetype)initWithDictionary:(NSDictionary *)dic;

//获取网络数据，参数是传出去的Block
-(void)getWeather:(void (^)(NSMutableArray<Weather*> *))callback;

@end

NS_ASSUME_NONNULL_END
