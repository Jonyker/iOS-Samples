//
//  WeatherCell.h
//  iOS-Network-Weather
//
//  Created by 吴开杰 on 2020/3/12.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Weather.h"
NS_ASSUME_NONNULL_BEGIN

@interface WeatherCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *temperature;
@property (weak, nonatomic) IBOutlet UILabel *weaher;
@property (weak, nonatomic) IBOutlet UILabel *wind;

@property(nonatomic,strong) Weather *w;

@end

NS_ASSUME_NONNULL_END
