//
//  WeatherCell.m
//  iOS-Network-Weather
//
//  Created by 吴开杰 on 2020/3/12.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "WeatherCell.h"
#import "Weather.h"
@implementation WeatherCell

- (void) setW:(Weather*) w{
    
    _w = w;
    self.date.text = w.date_y;
    self.temperature.text = w.temperature;
    self.weaher.text = w.weather;
    self.wind.text = w.wind;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
