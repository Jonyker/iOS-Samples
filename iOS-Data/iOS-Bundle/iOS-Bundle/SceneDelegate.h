//
//  SceneDelegate.h
//  iOS-Bundle
//
//  Created by 吴开杰 on 2020/3/9.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

