//
//  ViewController.m
//  iOS-Bundle
//
//  Created by 吴开杰 on 2020/3/9.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tfUserName;
@property (weak, nonatomic) IBOutlet UITextField *tfPassWord;
@property (weak, nonatomic) IBOutlet UISwitch *sw;
- (IBAction)remember:(id)sender;
- (IBAction)login:(id)sender;

@property (strong, nonatomic) NSUserDefaults *userDefault;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // 程序中资源文件，会被统一打包到Bundle中
    // 获取程序中的info.plist文件路径
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];;
    NSLog(@"%@",path);
    
    
    // 沙盒是iOS程序中一个私有的存放数据的区域，程序之间不可以相互访问，其中三个顶级文件夹，Documents，Library，tmp
    // 沙盒的入口
    NSLog(@"%@",NSHomeDirectory());
    
    // tmp
    NSLog(@"%@",NSTemporaryDirectory());
    
    // 1.拼接的方式，Documents
    NSString *doc = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSLog(@"%@",doc);
    
    // 2.函数调用的方式，Documents
    NSString *doc2 = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"%@",doc2);
    
    // Library
    NSString *lib = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"%@",lib);
    
    // Cache
    NSString *cache = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"%@",cache);
    
    // 解析plist文件
    NSDictionary *dic = [NSBundle mainBundle].infoDictionary;
    NSLog(@"%@",dic[@"CFBundleShortVersionString"]);
    // 获取自定义的plist文件
//    NSString *userPlistPath = [[NSBundle mainBundle] pathForResource:@"user" ofType:@"plist"];;
//    NSDictionary *userDic = [NSDictionary dictionaryWithContentsOfFile:userPlistPath];
//    NSLog(@"name = %@",userDic[@"name"]);
//    NSLog(@" pwd = %@",userDic[@"pwd"]);
    
    
//    // 解析plist文件
//    NSString *userPlistPath = [[NSBundle mainBundle] pathForResource:@"cityData.plist" ofType:nil];;
//    NSDictionary *cityDic = [NSDictionary dictionaryWithContentsOfFile:userPlistPath];
//    // 获取字典中key
//    NSString *key = [[cityDic allKeys] firstObject];
//    NSArray *array = cityDic[key];
//    NSLog(@"lastObject = %@",[array lastObject]);
//
//    NSString *cityFile = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"city.plist"];
//    NSLog(@"写入文件路径 = %@",cityFile);
//    [cityDic writeToFile:cityFile atomically:YES];
    
 
    // 文件会在沙盒中的Library->Preferences文件夹中的plist文件
    self.userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString* name = [self.userDefault valueForKey:@"name"];
    NSString* pwd = [self.userDefault valueForKey:@"pwd"];
    self.tfUserName.text = name;
    self.tfPassWord.text = pwd;
    
    Boolean isOn = [self.userDefault boolForKey:@"isOn"];
    self.sw.on = isOn;
    
    
}


- (IBAction)remember:(id)sender {
    UISwitch *sw = (UISwitch *)sender;
    if(sw.isOn){
        
        NSString * name = self.tfUserName.text;
        NSString * pwd = self.tfPassWord.text;
        
        [self.userDefault setObject:name forKey:@"name"];
        [self.userDefault setObject:pwd forKey:@"pwd"];
        
        // 还需要保存按钮的状态
        [self.userDefault setBool:sw.isOn forKey:@"isOn"];
        
        // 同步提交信息
        [self.userDefault synchronize];
        
    }
}

- (IBAction)login:(id)sender {
}
@end
