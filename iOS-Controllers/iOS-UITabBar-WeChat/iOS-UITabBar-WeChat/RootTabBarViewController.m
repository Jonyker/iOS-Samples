//
//  RootTabBarViewController.m
//  iOS-UITabBar-WeChat
//
//  Created by 吴开杰 on 2020/2/21.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "RootTabBarViewController.h"
#import "WechatController.h"
@interface RootTabBarViewController ()

@end

@implementation RootTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addChildWithVCName:@"WechatController" title:@"微信" image:@"index" selectImage:@"index_hl"];
    [self addChildWithVCName:@"ContactsController" title:@"联系人" image:@"contacts" selectImage:@"contacts_hl"];
    [self addChildWithVCName:@"DiscoverController" title:@"发现" image:@"discover" selectImage:@"discover_hl"];
    [self addChildWithVCName:@"MeController" title:@"我的" image:@"me" selectImage:@"me_hl"];
}


-(void)addChildWithVCName:(NSString *) vcName title:(NSString *) title image:(NSString *) image selectImage:(NSString *) selectImage{
    
    
    
    // 1.创建控制器
    Class class = NSClassFromString(vcName);
    UIViewController *wechat = [[class alloc]init];
    
    // 2.设置属性
    wechat.navigationItem.title = title;
    wechat.tabBarItem.title = title;
    wechat.tabBarItem.image = [UIImage imageNamed:image];
    wechat.tabBarItem.selectedImage = [UIImage imageNamed:selectImage];
    [wechat.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:37/255.0 green:170/255.0 blue:34/255.0 alpha:1.0]} forState:UIControlStateHighlighted];
    
    // 3.创建导航控制器
    UINavigationController *nvc = [[UINavigationController alloc]initWithRootViewController:wechat];
    nvc.navigationBar.barTintColor = [UIColor colorWithRed:15/255.0 green:15/255.0 blue:15/255.0 alpha:1.0];
    nvc.navigationBar.tintColor = [UIColor whiteColor];
    [nvc.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // 4.添加标签栏控制器
    [self addChildViewController:nvc];
    
    
}


@end
