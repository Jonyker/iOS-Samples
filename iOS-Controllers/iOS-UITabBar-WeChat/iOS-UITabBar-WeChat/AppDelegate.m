//
//  AppDelegate.m
//  iOS-UITabBar-WeChat
//
//  Created by 吴开杰 on 2020/2/21.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "AppDelegate.h"
#import "RootTabBarViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
    self.window.rootViewController = [[RootTabBarViewController alloc]init];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}


@end
