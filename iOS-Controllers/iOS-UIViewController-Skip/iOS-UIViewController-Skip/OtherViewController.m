//
//  OtherViewController.m
//  iOS-UIViewController-Skip
//
//  Created by 吴开杰 on 2020/2/19.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "OtherViewController.h"

@interface OtherViewController ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
- (IBAction)passeValueClick:(id)sender;

@end

@implementation OtherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.infoLabel.text = self.passseValue;
    
    
    // 控制器逆向传值
    // 1.代理逆向传值
    // 2.Block传值
    // 3.通知传值
    
    
    // 1.代理传值
    // 代理对象：主动去传值的对象，被代理对象：需要值得对象
        
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)passeValueClick:(id)sender {
    
    
    // 3.传值（第一步，第二部，是在头文件中定义协议和定义属性）
    [self.delegate passValue:@"逆向传值内容abc123"];
    
    
    // 3.Block完成传值
    if(self.passseValueBlock){
        self.passseValueBlock(@"逆向传值内容abc456");
    }
    
    
    // 通知的方式，将值传递到监听事件的位置
    [[NSNotificationCenter defaultCenter]postNotificationName:@"qwer" object:nil userInfo:@{@"key":@"逆向传值内容abc789"}];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
    
}
@end






