//
//  AppDelegate.h
//  iOS-UIViewController-Skip
//
//  Created by 吴开杰 on 2020/2/19.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

