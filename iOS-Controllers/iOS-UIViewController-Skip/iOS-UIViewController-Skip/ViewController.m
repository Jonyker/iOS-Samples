//
//  ViewController.m
//  iOS-UIViewController-Skip
//
//  Created by 吴开杰 on 2020/2/19.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"
#import "OtherViewController.h"

@interface ViewController ()<passValueProtocol>

- (IBAction)skipOtherUI:(id)sender;
- (IBAction)skipOtherUIWithLine:(id)sender;
- (IBAction)showAlert:(id)sender;
- (IBAction)showAlertSheet:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handNotify:) name:@"qwer" object:nil];
    
    
}
// 收到通知触发的事件
-(void)handNotify:(NSNotification *) noti{

    NSDictionary *userInfo = noti.userInfo;
    NSLog(@"%@",userInfo[@"key"]);
    self.infoLabel.text = userInfo[@"key"];

}


- (IBAction)skipOtherUI:(id)sender {
    
    // 跳转方式一：
    OtherViewController *svc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"other"];
    
    // 将需要传值的属性放在目标Controller中的w头文件中
    svc.passseValue = @"传值abc";
    // 第一步（第二步是下面实现代理的方法）
    // 很重要，让当前Controller成为代理，才能逆向传值
    svc.delegate = self;
    
    [self presentViewController:svc animated:YES completion:nil];
  
}

- (IBAction)skipOtherUIWithLine:(id)sender {
    // 跳转方式二：
    // 操作方式：在stroryboard中，选中ViewController，然后连线到目标ViewController界面，并且可以为连线设置id
    // 选择菜单：Present Modlly
    [self performSegueWithIdentifier:@"abcd" sender:nil];
       
    // 方式三：直接在界面连接，拖到连一个界面选择Present Modlly
    
}

// Stroryboard中的连线
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    // segue就是那条连接线的对象，然后通过连线，拿到目标Controller
    OtherViewController *other = segue.destinationViewController;
    other.passseValue = @"传值abc-通过Segue";
    other.passseValueBlock = ^(NSString* _Nonnull info){
        NSLog(@"%@",info);
        self.infoLabel.text = info;
    };
    
}

// Alert弹窗
- (IBAction)showAlert:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"注意，注意" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* _Nonnull action){
        NSLog(@"您点击了OK");
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction* _Nonnull action){
        NSLog(@"您点击了Cancel");
    }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    
    [self presentViewController: alert animated:YES completion:nil];

    
}
// 底部弹窗
- (IBAction)showAlertSheet:(id)sender {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"设置头像" message:@"选择方式" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *one = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction* _Nonnull action){
        NSLog(@"您点击了相册");
    }];
    
    UIAlertAction *two = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDestructive handler:^(UIAlertAction* _Nonnull action){
        NSLog(@"您点击了相册");
    }];
    
    UIAlertAction *three = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction* _Nonnull action){
        NSLog(@"您点击了相册");
    }];
    
    [alert addAction:one];
    [alert addAction:two];
    [alert addAction:three];
    [self presentViewController: alert animated:YES completion:nil];
    
}
// 第二步
// 实现逆向代理协议中的方式，有点像java中的接口回调
- (void)passValue:(NSString *)value{
    
    NSLog(@"%@",value);
    
    self.infoLabel.text = value;
    
}
@end
