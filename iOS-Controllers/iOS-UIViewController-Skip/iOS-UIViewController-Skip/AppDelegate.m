//
//  AppDelegate.m
//  iOS-UIViewController-Skip
//
//  Created by 吴开杰 on 2020/2/19.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
       
    //     // 创建指定rootViewController的方式一：（直接代码中创建）
    //     // 纯代码中设置UIViewController
    //     // 1.创建显示的Window，Window的大小和屏幕一样大
    //     self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    //     // 2.创建一个UIViewController
    //     UIViewController *vc  =  [[UIViewController alloc]init];
    //     vc.view.backgroundColor = UIColor.redColor;
    //     // 3.关联Window 和 UIViewController
    //     self.window.rootViewController = vc;
    //     // 4.将Window显示出来
    //     [self.window makeKeyAndVisible];

        
        
        
        
        
//        // 创建指定rootViewController的方式二：（stroryboard中创建）
//        // 1.创建显示的Window，Window的大小和屏幕一样大
//        self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
//        
//        // 2.找到stroryboard
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
//        
//        // 3.在stroryboard中设置标志，然后在代码中寻找
//        // UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"abc"];
//        // 或者寻找stroryboard中箭头指向的UIViewController
//        // UIViewController *vc = [sb instantiateInitialViewController];
//        // 或者使用xib
//        // XIBViewController *vc = [[XIBViewController alloc]initWithNibName:@"XIBViewController" bundle:nil];
//        XIBViewController *vc = [[XIBViewController alloc]init];
//        // 4.关联Window 和 UIViewController
//        self.window.rootViewController = vc;
//        
//        // 5.将Window显示出来
//        [self.window makeKeyAndVisible];
    
    return YES;
}

 


@end
