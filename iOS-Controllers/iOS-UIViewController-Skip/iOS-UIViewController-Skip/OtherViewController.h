//
//  OtherViewController.h
//  iOS-UIViewController-Skip
//
//  Created by 吴开杰 on 2020/2/19.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

// 1.定义协议
@protocol passValueProtocol <NSObject>

-(void)passValue:(NSString *) value;

@end

// 1.定义Block
typedef void (^ PassValueBlock)(NSString * info);


@interface OtherViewController : ViewController

// 2.定义Block属性
@property(nonatomic,copy) PassValueBlock passseValueBlock;

@property(nonatomic,copy) NSString * passseValue;

// 2.定义协议属性
@property(nonatomic,assign) id<passValueProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
