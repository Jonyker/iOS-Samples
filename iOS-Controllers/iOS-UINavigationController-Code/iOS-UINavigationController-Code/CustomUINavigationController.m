//
//  CustomUINavigationController.m
//  iOS-UINavigationController-Code
//
//  Created by 吴开杰 on 2020/2/20.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "CustomUINavigationController.h"

@interface CustomUINavigationController ()

@end

@implementation CustomUINavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    
    viewController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [super pushViewController:viewController animated:animated];
    
}
@end
