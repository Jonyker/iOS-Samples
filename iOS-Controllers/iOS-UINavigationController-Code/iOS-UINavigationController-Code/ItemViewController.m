//
//  ItemViewController.m
//  iOS-UINavigationController-Code
//
//  Created by 吴开杰 on 2020/2/20.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ItemViewController.h"

@interface ItemViewController ()

@end

@implementation ItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"详情";
    
    // 设置了leftBarButtonItem会把返回按钮和事件弄消失
    UIBarButtonItem *left = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_premium_splash_close"] style:UIBarButtonItemStylePlain target:self action:@selector(leftClick:)];

        self.navigationItem.leftBarButtonItem = left;
        
    }

-(void)leftClick:(UIBarButtonItem *) left{
    NSLog(@"-----");

    [self.navigationController popViewControllerAnimated:YES];

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    // 直接返回rootViewController
    // [self.navigationController popToRootViewControllerAnimated:YES];
    
    // 返回指定的UIViewController，注意她会把它上面的vc全部pop出去
    UIViewController *vc = self.navigationController.childViewControllers[0];
    [self.navigationController popToViewController:vc animated:YES];
    
    
}


@end
