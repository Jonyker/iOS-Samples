//
//  AppDelegate.m
//  iOS-UINavigationController-Code
//
//  Created by 吴开杰 on 2020/2/20.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "AppDelegate.h"

#import "CustomUINavigationController.h"
 
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"abc"];
    vc.navigationItem.title = @"首页";
    
    CustomUINavigationController *navi = [[CustomUINavigationController alloc]initWithRootViewController:vc];
    
    self.window.rootViewController = navi;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}





@end
