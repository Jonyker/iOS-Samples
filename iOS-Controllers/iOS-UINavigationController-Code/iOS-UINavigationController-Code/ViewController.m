//
//  ViewController.m
//  iOS-UINavigationController-Code
//
//  Created by 吴开杰 on 2020/2/20.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"
#import "ListViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithTitle:@"列表" style:UIBarButtonItemStylePlain target:self action:@selector(rightClick:)];
    
    self.navigationItem.rightBarButtonItem = right;
    
    
    // 点击的时候导航器消失，再次点击会出现
    self.navigationController.hidesBarsOnTap = YES;
    // 滑动消失导航器，不会出现
    self.navigationController.hidesBarsOnSwipe = YES;
    // 键盘弹出，导航条消失，不会出现
    self.navigationController.hidesBarsWhenKeyboardAppears = YES;
    // 屏幕旋转，导航条消失，旋转回来，还会出现
    self.navigationController.hidesBarsWhenVerticallyCompact = YES;
    
    
    
    
}

-(void)rightClick:(UIBarButtonItem *) right{
    NSLog(@"-----");
    
    
    ListViewController *listVC = [[ListViewController alloc]init];
    [self.navigationController pushViewController:listVC animated:YES];
    
    
}

@end
