//
//  ListViewController.m
//  iOS-UINavigationController-Code
//
//  Created by 吴开杰 on 2020/2/20.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ListViewController.h"
#import "ItemViewController.h"

@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"列表";
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithTitle:@"项目" style:UIBarButtonItemStylePlain target:self action:@selector(rightClick:)];

    self.navigationItem.rightBarButtonItem = right;
    
}



-(void)rightClick:(UIBarButtonItem *) right{
    NSLog(@"-----");


    ItemViewController *itemVC = [[ItemViewController alloc]init];
    [self.navigationController pushViewController:itemVC animated:YES];


}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    // 普通的返回
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
