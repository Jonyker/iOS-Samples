//
//  ViewController.m
//  iOS-UITabBar
//
//  Created by 吴开杰 on 2020/2/20.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITabBarDelegate>
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.tabBar.barTintColor  = [UIColor whiteColor];
    // self.tabBar.tintColor = [UIColor greenColor];
    self.tabBar.tintColor = [UIColor colorWithRed:34/255.0 green:172/255.0 blue:37/255.0 alpha:1.0];
    
    
    self.tabBar.items[0].badgeValue = @"13";
    // 设置选中
    self.tabBar.selectedItem = self.tabBar.items[0];
    
    
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    NSLog(@"%@", item.title);
    
    
    
    // 清楚红色消息角标
    item.badgeValue = nil;
    
    
    
}


@end
