//
//  CustomTabBarController.m
//  iOS-UITabBar
//
//  Created by 吴开杰 on 2020/2/21.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "CustomTabBarController.h"

@interface CustomTabBarController ()<UITabBarControllerDelegate>

@end

@implementation CustomTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

// 选中的那个item
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    
}

// 选中的那个item对应的ViewController
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    viewController.tabBarItem.badgeValue = nil;
    
}


@end
