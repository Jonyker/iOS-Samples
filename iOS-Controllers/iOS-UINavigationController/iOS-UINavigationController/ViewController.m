//
//  ViewController.m
//  iOS-UINavigationController
//
//  Created by 吴开杰 on 2020/2/20.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)leftEdit:(id)sender;
- (IBAction)rightLocation:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    self.navigationItem.title = @"首页";
    
    UIImageView *imgView =[[UIImageView alloc]initWithImage: [UIImage imageNamed:@"image_dialog_diskanalyzer"]];
    imgView.bounds = CGRectMake(0, 0, 32, 32);
    self.navigationItem.titleView = imgView;
    
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"abc"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


- (IBAction)leftEdit:(id)sender {
    
    NSLog(@"编辑");
    
    
}

- (IBAction)rightLocation:(id)sender {
    NSLog(@"定位");
    
}
@end
