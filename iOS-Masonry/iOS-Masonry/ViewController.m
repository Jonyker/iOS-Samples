//
//  ViewController.m
//  iOS-Masonry
//
//  Created by 吴开杰 on 2020/3/2.
//  Copyright © 2020 吴开杰. All rights reserved.
//

#import "ViewController.h"

#import "Masonry.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

//    //  蓝色View
//    UIView *blueView = [[UIView alloc] init];
//    blueView.backgroundColor = [UIColor blueColor];
//    [self.view addSubview:blueView];
//
//    //  红色View
//    UIView *redView = [[UIView alloc] init];
//    redView.backgroundColor = [UIColor redColor];
//    [self.view addSubview:redView];
//
//    //  bluView的约束
//    [blueView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.view).offset(20);
//        make.right.equalTo(self.view).offset(-20);
//        make.top.equalTo(self.view).offset(20);
//        make.height.equalTo(@50);
//    }];
//
//    //  redView的约束
//    [redView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(blueView.mas_centerX);
//        make.right.equalTo(blueView.mas_right);
//        make.top.equalTo(blueView.mas_bottom).offset(20);
//        make.height.equalTo(blueView.mas_height);
//    }];
 
    
    
    
    
    
    
    

//    UIView *sv = [UIView new];
//    sv.backgroundColor = [UIColor blackColor];
//
//    [self.view addSubview:sv];
//    [sv mas_makeConstraints:^(MASConstraintMaker *make) {
//
//        make.left.right.equalTo(@0);
//        make.height.equalTo(@200);
//        make.center.equalTo(self.view);
//
//    }];
//
//    UIView *sv1 = [UIView new];
//    sv1.backgroundColor = [UIColor redColor];
//    [sv addSubview:sv1];
//    [sv1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        // 第一种方式
//        //    make.top.equalTo(sv).with.offset(10);
//        //    make.left.equalTo(sv).with.offset(10);
//        //    make.right.equalTo(sv).with.offset(-10);
//        //    make.bottom.equalTo(sv).with.offset(-10);
//        //第二种方法
//        //基于某个view四周进行约束
//        //    make.edges.equalTo(sv).insets(UIEdgeInsetsMake(20, 10, 20, 10));
//        //
//        //第三种方法
//        make.top.and.left.and.bottom.and.right.equalTo(sv).with.insets(UIEdgeInsetsMake(10, 10, 10, 10));
//
//    }];
//
//
//    int padding1 = 10;
//    UIView *sv2 = [UIView new];
//    sv2.backgroundColor = [UIColor colorWithRed:0.000 green:1.000 blue:0.502 alpha:1.000];
//    [self.view addSubview:sv2];
//
//    UIView *sv3 = [UIView new];
//    sv3.backgroundColor = [UIColor colorWithRed:0.000 green:1.000 blue:0.502 alpha:1.000];
//    [self.view addSubview:sv3];
//
//    [sv2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.mas_equalTo(150);
//        make.centerY.mas_equalTo(sv.mas_centerY);
//        make.width.equalTo(sv3);
//        make.left.equalTo(sv).with.offset(padding1);
//        make.right.equalTo(sv3.mas_left).with.offset(-padding1);
//
//    }];
//
//    [sv3 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.mas_equalTo(@150);
//        make.centerY.mas_equalTo(sv.mas_centerY);
//        make.width.equalTo(sv2);
//        make.left.equalTo(sv2.mas_right).with.offset(padding1);
//        make.right.equalTo(sv.mas_right).with.offset(-padding1);
//
//    }];
    
    
    
    
    
    
    UIView *blackView = [UIView new];
    blackView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:blackView];
    
    UIView *grayView = [UIView new];
    grayView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:grayView];
    
    [blackView mas_makeConstraints:^(MASConstraintMaker *make) {
        //添加左,上边距的约束
        //mas_equal的支持CGSIZE CGPOINT NSNUMBER UIEDGEINSETS
        make.top.mas_equalTo(80);
        make.left.mas_equalTo(20);
        //添加右边约束
        make.right.mas_equalTo(-20);
    }];
    
    [grayView mas_makeConstraints:^(MASConstraintMaker *make) {
        //添加右下约束
        make.bottom.right.mas_equalTo(-20);
        //添加高度约束,让高度等于blackview
        make.height.equalTo(blackView);
        //添加上边约束
        make.top.equalTo(blackView.mas_bottom).offset(20);
        //添加左边距
        make.left.equalTo(self.view.mas_centerX).offset(0);
    }];
    
 
}


@end
